'use strict';

var loopback = require('loopback');
var deviceTracker = loopback.getModel('deviceTracker');
var monitored = loopback.getModel('monitored');

module.exports = function (Monitored) {


    /**
 * 
 * @param {number} id MonitorId
 * @param {number} trackerId TrackerId
 * @param {Function(Error)} callback
 */
	Monitored.addDeviceById = function (id, trackerId, callback) {
		console.log(id, trackerId);
		deviceTracker.findOne({ where: { id: trackerId } }, function (error, trackerInstance) {

			if (trackerInstance) {

				monitored.findOne({ where: { id: id } }, function (err, instance) {
					if (instance) {
						/**
						 * Set the Monitored to The device
						 */
						/*trackerInstance.updateAttributes(
							{
								monitored: instance
							},
							function (err, succ) {

							})*/
						/**
						 * Set the the device to The Monitored
						 */
						instance.updateAttributes(
							{
								device: trackerInstance
							},
							function (err, succ) {
								callback(err, succ)
							})
					} else {
						callback(err, instance)
					}
				});

			} else {
				callback(error, trackerInstance)
			}
		});
	};

};
