'use strict';

module.exports = function (app, cb) {

  const ALERTS = [
    'MoveIn',
    'MoveOut',
    'GeoIn',
    'GeoOut',
    'OverSpeed',
    'LowSpeed',
    'Help',
    'VIB',
    'LowBattery',
    'ChargerON',
    'ChargerOFF',
    'BeltUp',
    'Belt Off',
    'Hit',
    'Home',
    'Leave'
  ]

  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   */

  var Monitored = app.models.monitored;
  var TrackLogs = app.models.trackLogs;
  var net = require('net');
  var EventEmitter = require("events").EventEmitter;
  var theEvent = new EventEmitter();

  var server = net.createServer(function (sock) {
    console.log('Server Net Connected');//server connected log
    sock.setEncoding('ascii'); sock.setEncoding('ascii');//Format data

    sock.on("error", function (error) {
      console.log("Caught flash policy server socket error: ");
      console.log(error.stack);
    })

    sock.on('data', function (data) {
      var converted = data.split(',');
      console.log(converted);
      var IMEI = converted[17].replace('imei:', '');
      var NS = converted[6];
      var EW = converted[8];
      var battery = converted[20].replace('Battery=', '');

      var Lat = getLatLong(converted[5], NS);
      var Long = getLatLong(converted[7], EW);

      Monitored.findOne({ include: { relation: 'deviceTrackers', scope: { where: { and: [{ 'IMEI': IMEI }, { status: 'Active' }] } } } }, function (error, success) {

        TrackLogs.findOne({ "order": "id DESC" }, function (err, track) {

          if (typeof success.toJSON().deviceTrackers !== 'undefined') {
            track = (track) ? track.toJSON() : null
            var alertsProcessed = processAlerts(track, converted);

            var deviceDataProcesed = {
              IMEI: IMEI,
              LatLng: { lat: Lat, lng: Long },
              chargeOn: alertsProcessed.chargeOn,
              beltStatus: alertsProcessed.beltStatus,
              battery: battery,
              deviceTrackerId: success.toJSON().deviceTrackers.id
            }

            TrackLogs.create(deviceDataProcesed, function (err, succ) {
              console.log(err, succ);
              var toMap = success.toJSON();
              toMap.deviceTrackers = succ.toJSON()
              theEvent.emit('deviceGPS', toMap);
            });
          }
        });

      })

    })
  });
  server.listen(1038, 'localhost');

  theEvent.on('deviceGPS', function (data) {
    app.io.emit('GPS', data);
  });

  function getLatLong(LatLong, dir) {
    var value = Number(LatLong);
    var degrees = Number(value.toString().substr(0, 2));
    var minutes = Number(value.toString().replace(degrees, ''));

    var res = degrees + (minutes / 60);
    if (dir == 'S' || dir == 'W') {
      res = res * -1;
    }

    return res;
  }

  function processAlerts(track, converted) {
    if (track) {
      if (typeof ALERTS[converted[16]] != 'undefined') {

        if (converted[16] == 'BeltUp' || converted[16] == 'Belt Off') {
          track.beltStatus = converted[16];
        } else if (converted[16] == 'ChargerON' || converted[16] == 'ChargerOFF') {
          track.chargeOn = converted[16];
        }
      }
    } {
      track = {
        beltStatus: 'BeltUp',
        chargeOn: 'ChargerOFF'
      }

      if (converted[16] == 'BeltUp' || converted[16] == 'Belt Off') {
        track.beltStatus = converted[16];
      } else if (converted[16] == 'ChargerON' || converted[16] == 'ChargerOFF') {
        track.chargeOn = converted[16];
      }
    }

    return track;
  }

  process.setMaxListeners(0);

  process.nextTick(cb);
};
